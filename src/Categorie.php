<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author dev
 */
namespace Eshop;
class Categorie {

    private $idCategorie;
    private $libelle;
    private $image;

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getIdCategorie() {
        return $this->idCategorie;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    private static $select = "select * from categorie ";
    private static $selectById = "select * from categorie where
idCategorie=:idCategorie";
    private static $insert = "insert into categorie (libelle, image) values
(:libelle,:image)";
    private static $update = "update categorie set libelle=:libelle,
image=:image where idCategorie = :idCategorie";

    public function save() {
        if ($this->idCategorie == null) {
            $this->insert();
        } else {
            $this->update();
        }
        $this->saveProduits();
    }

    public function insert() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->execute();
        $this->idCategorie = $pdo->lastInsertId();
    }

    public function update() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$update);
        $pdoStatement->bindParam("idCategorie", $this->idCategorie);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->execute();
    }

    public static function fetch($idCategorie) {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$selectById);
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $categorie = Categorie::arrayToCategorie($record);
        return $categorie;
    }

    public static function fetchAll() {
        $categories = array();
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->query(Categorie::$select);
        $resultat = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        if (is_array($resultat)) {
            foreach ($resultat as $value) {
                $categories[] = Categorie::arrayToCategorie($value);
            }
        }
        return $categories;
    }

    private static function arrayToCategorie(Array $array) {
        $c = new Categorie();
        $c->idCategorie = $array["idCategorie"];
        $c->libelle = $array["libelle"];
        $c->image = $array["image"];
        $c->collectionProduit = Produit::fetchAllByCategorie($c);
        return $c;
    }

    private $collectionProduit = array();

    public function compareTo(Categorie $categorie) {
        return $this->idCategorie == $categorie->idCategorie;
    }

    private function existProduit(Produit $produit) {
        $existe = false;
        foreach ($this->collectionProduit as $produitCourant) {
            if ($produit->compareTo($produitCourant)) {
                $existe = true;
                break;
            }
        }
        return $existe;
    }

    private function saveProduits() {
        foreach ($this->collectionProduit as $produit) {
            $produit->save();
        }
    }

    public function addProduit(Produit $produit) {
        if ($this->existProduit($produit) == false) {
            $this->collectionProduit[] = $produit;
            if (is_a($produit->getCategorie(), "Categorie") &&
                    !$produit->getCategorie()->compareTo($this)) {
                $produit->setCategorie($this);
            }
        }
    }

    public function removeProduit(Produit $produit) {
        $new = array();
        foreach ($this->collectionProduit as $produitCourant) {
            if (!$produitCourant->compareTo($produit)) {
                $new[] = $produitCourant;
                break;
            }
        }
        $this->collectionProduit = $new;
        $produit->setCategorie(null);
    }

}

